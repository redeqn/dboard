# dboard

Discussion board implementation using Java on AppEngine with Google Data Store backend.

Each supported feature of the API can be tested by filling in the provided text boxes. A URL automatically created using the information in the textboxes. Click on the link to execute the API call in your browser. Alternatively you can use the CURL command.

Two separate methods used for pagination. One is standard offset -- the problem with offset is that it does not scale well for very large data-sets. Offset only limits the number of records returned from the dataset. The DBMS still needs to go through the records to figure out the records to be returned.

The second one is Cursors which is a much better way to handle pagination on large datasets. However, it comes with limitations. One of which is that we cannot slice the query results at an arbitrary location. It simply allows us to go forward (or backward) from a cursor provided by a query that was run previously.

For topic list, we use cursors. For message list we use offsets.

