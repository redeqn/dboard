import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;

public class Topic extends Msg {
	public String title;

	public static Topic FromEntity(Entity e) {
		Topic resp = new Topic();
		resp.active = ((Date) e.getProperty("active")).getTime();
		resp.nickname = (String) e.getProperty("nickname");
		// normally I would not return the email with the messages
		// but otherwise email is doing nothing -- not used anywhere.
		resp.email = (String) e.getProperty("email");
		resp.content = (String) e.getProperty("content");
		resp.title = (String) e.getProperty("title");
		return resp;
	}

	public static void APICreate(HttpServletRequest req, HttpServletResponse resp) {
		String title = Utils.param(req, "title");
		if (title.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "title");
			return;
		}

		Msg.APICreate(req, resp, "Topic", title);
	}

	public static void APIUpdate(HttpServletRequest req, HttpServletResponse resp) {
		Msg.APIUpdate(req, resp, "Topic");
	}

	public static void APIDelete(HttpServletRequest req, HttpServletResponse resp) {
		Msg.APIDelete(req, resp, "Topic");
	}

	public static void APIList(HttpServletRequest req, HttpServletResponse resp) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Query q = new Query("Topic").addSort("active", SortDirection.DESCENDING);

		int limit = 100; // see if user provided a limit
		String limitS = Utils.param(req, "limit");
		if (!limitS.equals("")) {
			limit = Integer.parseInt(limitS);
		}

		if (limit > 100) { // make sure to set an upperbound for the limit
			limit = 100;
		}

		FetchOptions fetchopts = FetchOptions.Builder.withLimit(limit);

		// see if user provided a cursor
		String cursor = req.getParameter("cursor");
		if (cursor != null && !cursor.equals("")) {
			fetchopts.startCursor(Cursor.fromWebSafeString(cursor));
		}

		PreparedQuery pq = datastore.prepare(q);

		QueryResultList<Entity> results;
		try {
			results = pq.asQueryResultList(fetchopts);
		} catch (IllegalArgumentException e) {
			Response.ErrorResponse(resp, "Invalid cursor");
			return;
		}

		ArrayList<Msg> lst = new ArrayList<Msg>();
		for (Entity e : results) {
			lst.add(Topic.FromEntity(e));
		}

		MsgList respList = new MsgList();
		respList.messages = lst.toArray(new Msg[lst.size()]);

		String ncursor = results.getCursor().toWebSafeString();
		if (cursor == null || !cursor.equals(ncursor)) {
			if (results.getCursor() != null) {
				respList.cursor = results.getCursor().toWebSafeString();
			}
		}

		Response.SuccessList(resp, respList);
	}
}
