import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DiscussionBoard", urlPatterns = { "/api/*" })
public class DBoard extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		doGet(req, resp);
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");

		String path = req.getPathInfo();
		String[] parts = path.split("/");
		if (parts.length != 3) {
			Response.ErrorResponse(resp, "Invalid API path");
			return;
		}

		String verS = parts[1];
		if (!verS.startsWith("v")) {
			Response.ErrorResponse(resp, "Invalid API version");
			return;
		}

		String cmd = parts[2];
		switch (cmd) {
		case "createtopic":
			Topic.APICreate(req, resp);
			break;
		case "updatetopic":
			Topic.APIUpdate(req, resp);
			break;
		case "deletetopic":
			Topic.APIDelete(req, resp);
			break;
		case "listtopics":
			Topic.APIList(req, resp);
			break;
		case "createmsg":
			Msg.APICreate(req, resp);
			break;
		case "updatemsg":
			Msg.APIUpdate(req, resp);
			break;
		case "deletemsg":
			Msg.APIDelete(req, resp);
			break;
		case "listmsgs":
			Msg.APIList(req, resp);
			break;
		default:
			Response.ErrorResponse(resp, "unknown request '" + cmd + "'.");
			break;
		}
	}
}