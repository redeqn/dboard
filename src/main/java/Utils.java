import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Utils {
	public static void output(HttpServletResponse resp, String msg) {
		try {
			resp.getWriter().print(msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String param(HttpServletRequest req, String name) {
		String param = req.getParameter(name);
		if (param == null) {
			param = "";
		}
		return param;
	}

	static boolean isValidEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
