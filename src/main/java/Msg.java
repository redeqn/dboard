import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;

public class Msg {
	public long active;
	public String content;
	public String nickname;
	public String email;

	public static Msg FromEntity(Entity e) {
		Msg resp = new Msg();
		resp.active = ((Date) e.getProperty("active")).getTime();
		resp.nickname = (String) e.getProperty("nickname");
		// normally I would not return the email with the messages
		// but otherwise email is doing nothing -- not used anywhere.
		resp.email = (String) e.getProperty("email");
		resp.content = (String) e.getProperty("content");
		return resp;
	}

	public static void APICreate(HttpServletRequest req, HttpServletResponse resp) {
		Msg.APICreate(req, resp, "Msg", null);
	}

	protected static void APICreate(HttpServletRequest req, HttpServletResponse resp, String entity, String title) {
		String content = Utils.param(req, "content");
		if (content.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "content");
			return;
		}

		String nickname = Utils.param(req, "nickname");
		if (nickname.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "nickname");
			return;
		}

		String email = Utils.param(req, "email");
		if (email.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "email");
			return;
		}

		if (!Utils.isValidEmail(email)) {
			Response.ErrorResponse(resp, "Invalid email address");
			return;
		}

		String topic = Utils.param(req, "topic");
		if (entity.equals("Msg")) { // if trying to create message, then complain if no topic provided
			if (topic.equalsIgnoreCase("")) {
				Response.ErrorMissingField(resp, "topic");
				return;
			}
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		String secret = Utils.param(req, "secret");

		if (secret.equalsIgnoreCase("")) { // create new
			secret = java.util.UUID.randomUUID().toString();
			Entity e = new Entity(entity, secret);
			e.setProperty("date", new Date());
			e.setProperty("active", new Date());
			e.setProperty("content", content);
			e.setProperty("nickname", nickname);
			e.setProperty("email", email);

			// messages have topic field, topics have title field
			if (entity.equals("Topic")) {
				e.setProperty("title", title);
			} else {
				e.setProperty("topic", topic);
			}

			datastore.put(e);
			Response.SuccessCreate(resp, secret);
		} else { // update

		}
	}

	public static void APIUpdate(HttpServletRequest req, HttpServletResponse resp) {
		Msg.APIUpdate(req, resp, "Msg");
	}

	protected static void APIUpdate(HttpServletRequest req, HttpServletResponse resp, String entity) {
		String content = Utils.param(req, "content");
		if (content.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "content");
			return;
		}

		// limit content length
		if (content.length() > 256) {
			content = content.substring(0, 256);
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		String secret = Utils.param(req, "secret");

		Key key = KeyFactory.createKey(entity, secret);
		Entity e;
		try {
			e = datastore.get(key);
		} catch (EntityNotFoundException ex) {
			Response.ErrorResponse(resp, "No record with secret " + secret + " found");
			return;
		}
		e.setProperty("active", new Date());
		e.setProperty("content", content);
		datastore.put(e);

		// for messages update my topic active time as well
		if (entity == "Msg") {
			String tsecret = (String) e.getProperty("topic");
			Key tkey = KeyFactory.createKey("Topic", tsecret);

			Entity te;
			try {
				te = datastore.get(tkey);
			} catch (EntityNotFoundException ex) {
				Response.ErrorResponse(resp, "Internal error! No topic with secret " + secret + " found");
				return;
			}
			te.setProperty("active", new Date());
			datastore.put(te);
		}

		Response.SuccessResponse(resp);
	}

	public static void APIDelete(HttpServletRequest req, HttpServletResponse resp) {
		Msg.APIDelete(req, resp, "Msg");
	}

	// called for message and topic
	protected static void APIDelete(HttpServletRequest req, HttpServletResponse resp, String entity) {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		String secret = Utils.param(req, "secret");
		if (secret.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "secret");
			return;
		}
		Key key = KeyFactory.createKey(entity, secret);
		datastore.delete(key);
		Response.SuccessResponse(resp);
	}

	protected static void APIList(HttpServletRequest req, HttpServletResponse resp) {
		int limit = 100; // see if user provided a limit
		String limitS = Utils.param(req, "limit");
		if (!limitS.equals("")) {
			limit = Integer.parseInt(limitS);
		}

		if (limit > 100) { // make sure to set an upperbound for the limit
			limit = 100;
		}

		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		String topic = Utils.param(req, "topic");
		if (topic.equalsIgnoreCase("")) {
			Response.ErrorMissingField(resp, "topic");
			return;
		}

		int index = 0;
		String indexS = Utils.param(req, "index");
		if (!indexS.equalsIgnoreCase("")) {
			index = Integer.parseInt(indexS);
		}

		index = (int) Math.ceil(index - limit / 2);
		if (index < 0) {
			index = 0;
		}

		FetchOptions fetchopts = FetchOptions.Builder.withLimit(limit).offset(index);
		Query q = new Query("Msg").addSort("active", SortDirection.DESCENDING);
		q.setFilter(new FilterPredicate("topic", FilterOperator.EQUAL, topic)); // this topics messages
		List<Entity> msgs = datastore.prepare(q).asList(fetchopts);

		ArrayList<Msg> list = new ArrayList<Msg>();
		for (Entity e : msgs) {
			list.add(Msg.FromEntity(e));
		}

		MsgList response = new MsgList();
		response.messages = list.toArray(new Msg[list.size()]);
		Response.SuccessList(resp, response);
	}
}
