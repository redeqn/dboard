import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

public class Response {

	static class Error {
		String status = "error";
		String msg;

		Error(String msg) {
			this.msg = msg;
		}
	}

	public static void ErrorResponse(HttpServletResponse resp, String msg) {
		Gson gson = new Gson();
		Error e = new Error(msg);
		Utils.output(resp, gson.toJson(e));
	}

	public static void ErrorMissingField(HttpServletResponse resp, String field) {
		Response.ErrorResponse(resp, field + " is required.");
	}

	static class Success {
		String status = "success";
	}

	public static void SuccessResponse(HttpServletResponse resp) {
		Gson gson = new Gson();
		Success e = new Success();
		Utils.output(resp, gson.toJson(e));
	}

	static class SuccessCreate {
		String status = "success";
		String secret;

		SuccessCreate(String secret) {
			this.secret = secret;
		}
	}

	public static void SuccessCreate(HttpServletResponse resp, String secret) {
		Gson gson = new Gson();
		SuccessCreate e = new SuccessCreate(secret);
		Utils.output(resp, gson.toJson(e));
	}

	public static void SuccessList(HttpServletResponse resp, MsgList lst) {
		Gson gson = new Gson();
		Utils.output(resp, gson.toJson(lst));
	}
}
